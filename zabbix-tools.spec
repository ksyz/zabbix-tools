# https://git.in.o2bs.sk/mingeli/zabbix-tools
%define _external_scripts /var/lib/zabbixsrv/externalscripts/
%global commit0 25c75a08c4bce6af429008bbe32faf36dff9b20a
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})

Name:		zabbix-tools
Version:	1
Release:	1%{?dist}
Summary:	Zabbix goodies and rape tools

License:	WTFPL
URL:		https://git.in.o2bs.sk/mingeli/%{name}
Source0:	https://git.in.o2bs.sk/mingeli/%{name}/repository/archive.tar.gz?ref=%{commit0}#/%{name}-%{shortcommit0}.tar.gz

BuildArch:	noarch

Requires:	perl
Requires:	/var/lib/zabbixsrv/externalscripts/

%description
Zabbix server LLD and other goodies


%prep
%setup -q -n %{name}-%{commit0}-%{commit0}


%build
true


%install
install -d -m 755 %{buildroot}/%{_external_scripts}
install -m 755 lld-snmp-if %{buildroot}%{_external_scripts}
install -m 755 lld-snmp-cpu %{buildroot}%{_external_scripts}


%files
%attr(755, root, root) %{_external_scripts}/*


%changelog
* Tue Apr 19 2016 Michal Ingeli <michal.ingeli@o2.sk> 1-1
- first release
